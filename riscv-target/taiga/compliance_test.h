// RISC-V Compliance Test Header File
// Copyright (c) 2017, Codasip Ltd. All Rights Reserved.
// See LICENSE for license details.
//
// Description: Common header file for RV32I tests

#ifndef _COMPLIANCE_TEST_H
#define _COMPLIANCE_TEST_H

#include "riscv_test.h"

//-----------------------------------------------------------------------
// RV Compliance Macros
//-----------------------------------------------------------------------

#define UART_TX_REG 0x60001000
#define SIGNATURE_PUTC(_R)                                               	\
	li a0, UART_TX_REG; 																\
	sw _R, 0 (a0);
	
#define SIGNATURE_A                                              	\
	li a0, UART_TX_REG; \
	li t0, 85; \
	sw t0, 0 (a0);

//writes bytes out, formated by verilator testbench
#define RV_COMPLIANCE_HALT                                                    \
		fence; \
		nop; \
		li zero, 0xB; \
		fence; \
		nop; \
		la t0, taiga_signature_start; \
		la t1, taiga_signature_end; \
		beq t0, t1, 3001f; \
		li a0, UART_TX_REG; \
		la a1, char_table; \
3000: \
		lbu t2, 3 (t0); \
		srli t4, t2, 4; \
		add t4, t4, a1; \
		lbu t3, 0 (t4); \
		sw t3, 0 (a0); \
		andi t4, t2, 0xF; \
		add t4, t4, a1; \
		lbu t3, 0 (t4); \
		sw t3, 0 (a0); \
		\
		lbu t2, 2 (t0); \
		srli t4, t2, 4; \
		add t4, t4, a1; \
		lbu t3, 0 (t4); \
		sw t3, 0 (a0); \
		andi t4, t2, 0xF; \
		add t4, t4, a1; \
		lbu t3, 0 (t4); \
		sw t3, 0 (a0); \
		\
		lbu t2, 1 (t0); \
		srli t4, t2, 4; \
		add t4, t4, a1; \
		lbu t3, 0 (t4); \
		sw t3, 0 (a0); \
		andi t4, t2, 0xF; \
		add t4, t4, a1; \
		lbu t3, 0 (t4); \
		sw t3, 0 (a0); \
		\
		lbu t2, 0 (t0); \
		srli t4, t2, 4; \
		add t4, t4, a1; \
		lbu t3, 0 (t4); \
		sw t3, 0 (a0); \
		andi t4, t2, 0xF; \
		add t4, t4, a1; \
		lbu t3, 0 (t4); \
		sw t3, 0 (a0); \
		\
		li t3, 10; \
		sw t3, 0 (a0); \
		addi t0, t0, 4; \
		blt t0, t1, 3000b; \
3001: \
		fence; \
		lbu t3, 0 (t4); \
		addi x0, x0, 0xA; \
3002: \
		j 3002b;

#define RV_COMPLIANCE_RV32M                                                   \
                                                                              \

#define RV_COMPLIANCE_CODE_BEGIN                                              \
        .section .text.init;                                                  \
        .align  4;                                                            \
        .globl _start;                                                        \
        _start:                                                               \

#define RV_COMPLIANCE_CODE_END                                                \
                                                                              \

#define RV_COMPLIANCE_DATA_BEGIN                                              \
        .align 4;                                                             \
        .global taiga_signature_start;                                      \
        taiga_signature_start:                          \


#define RV_COMPLIANCE_DATA_END                                                \
        .align 4;                                                             \
        .global taiga_signature_end;                                        \
        taiga_signature_end: 	\
        .align 8; .global begin_regstate; begin_regstate:               \
        .word 128;                                                      \
        .align 8; .global end_regstate; end_regstate:                   \
        .word 4;	\
		.align 4; \
		char_table:    .byte   48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 97, 98, 99, 100, 101, 102
        
#endif

